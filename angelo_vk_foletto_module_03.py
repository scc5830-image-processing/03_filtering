# coding: utf-8

# Nome: Angelo Victor Kraemer Foletto
# Numero USP: 12620258
# Código do Curso: SCC5830 - Image Processing (2021) / 1
# Docente: Moacir Antonelli Ponti
# Assignment 3: filtering
# Data 26/05/2021


## Imports
import numpy as np
import imageio as imgio
import math


class Module03:

    def __init__(self, img, method, filter_weight, lineByLine=None):
        self.img = imgio.imread(img)
        self.method = method
        self.filter_weight = filter_weight
        if lineByLine is not None:
            self.lineByLine = lineByLine

    def splitLineByLine(self, line):
        """

        :param line: String
        :return: Integer List
        """
        if type(line) == str:
            return np.array(list(map(np.float64, line.split(' ')))).astype(np.float64)
        elif type(line) is list:
            for i in range(len(line)):
                line[i] = line[i].replace('+', '').split(' ')
            line = np.array(line).flatten()
            return np.resize(np.array(list(map(np.float64, line))).astype(np.float64),
                             (self.filter_weight, self.filter_weight))

    def normalize(self, values):
        imin = np.min(values)
        swap = (values - imin) / (np.max(values) - imin)
        return (swap * 255).astype(np.float64)

    def sizePad(self):
        return int(self.filter_weight / 2)

    def filter1D(self):
        """

        :return: new image array
        """
        self.lineByLine = self.splitLineByLine(self.lineByLine)
        size_pad = self.sizePad()
        swap_img = np.pad(self.img.flatten(), (size_pad, size_pad), 'wrap')
        result = np.zeros(swap_img.shape[0], dtype=np.float64)

        for i in range(size_pad, swap_img.flatten().shape[0] - size_pad):
            swap_i = i - size_pad
            swap_j = i + size_pad + 1
            result[swap_i] = np.sum(np.multiply(swap_img[swap_i: swap_j], self.lineByLine))

        result = result[size_pad:(result.shape[0] - size_pad)]
        return np.reshape(self.normalize(result), self.img.shape)

    def filter2D(self):
        """

        :return: new image array
        """
        self.lineByLine = self.splitLineByLine(self.lineByLine)
        size_pad = self.sizePad()
        swap_img = np.pad(self.img, [(size_pad, size_pad), (size_pad, size_pad)], 'reflect')
        size_matrix = swap_img.shape[0]
        result = np.zeros((size_matrix, size_matrix), dtype=np.float64)

        for i in range(self.filter_weight, size_matrix - self.filter_weight):
            swap_i = i - self.filter_weight
            for j in range(self.filter_weight, size_matrix - self.filter_weight):
                swap_j = j - self.filter_weight
                result[swap_i:swap_j, swap_i:swap_j] = np.sum(
                    np.multiply(swap_img[swap_i:i + 1, swap_j:j + 1], self.lineByLine))
        return self.normalize(result[size_pad:size_matrix - size_pad, size_pad:size_matrix - size_pad])

    def filter3D(self):
        """

        :return: new image array
        """
        size_pad = self.sizePad()
        swap_img = np.pad(self.img
                          , [(size_pad, size_pad), (size_pad, size_pad)]
                          , 'constant'
                          , constant_values=(0, 0))
        size_matrix = swap_img.shape[0]
        result = np.zeros((self.img.shape[0], self.img.shape[0]), dtype=np.float64)
        for i in range(self.filter_weight, size_matrix):
            swap_i = i - self.filter_weight
            for j in range(self.filter_weight, size_matrix):
                swap_j = j - self.filter_weight
                result[swap_i][swap_j] = np.sort(swap_img[swap_i:i, swap_j:j].flatten()
                                                )[int(np.power(self.filter_weight, 2) / 2)]
        return result

    def calcRMSE(self, original, new_image):
        """
        Calc RMSE
        """
        pw = 2
        return math.sqrt(
            np.sum(np.power(np.subtract(original, new_image), pw))
            / (new_image.shape[0] * new_image.shape[1])
        )

    def filterMethod(self):
        """

        :return: new_image numpy array is type unit8
        """
        if self.method == 1:
            return self.filter1D()
        elif self.method == 2:
            return self.filter2D()
        elif self.method == 3:
            return self.filter3D()

    def main(self):
        """

        :return: Return RMSE calc
        """
        return self.calcRMSE(self.img, self.filterMethod())


if __name__ == '__main__':

    ## Input test case
    weights = str()
    img_name = str(input()).rstrip()  # Image name
    choice_method = int(input())  # Choice of method filter
    size = int(input())  # Size of filter
    if choice_method == 1:
        weights = str(input().strip())  # Filter weights
    elif choice_method == 2:
        weights = list()
        for i in range(size):
            weights.append(str(input().strip()))
    module = Module03(img_name, choice_method, size, weights)

    ### -- deploy - debug -- ###
    # img_name = 'ImagensParaCasosDeTeste-2/image01.jpg'
    # img_name = 'ImagensParaCasosDeTeste-2/image02_quant.png'
    # img_name = 'ImagensParaCasosDeTeste-2/flower.png'
    # img_name = 'ImagensParaCasosDeTeste-2/image02_salted.png'
    # img_name = 'ImagensParaCasosDeTeste-2/camera_saltpepper.png'
    # module = Module03(img_name, 1, 7, '-3 -2 -1 0 1 2 3')
    # module = Module03(img_name, 2, 3, ['-1 0 1', '-2 0 +2', '-1 0 1'])
    # module = Module03(img_name, 2, 5, ['-1 -1 -1 -1 -1', '-1 -1 -1 -1 -1', '-1 -1 24 -1 -1', '-1 -1 -1 -1 -1', '-1 -1 -1 -1 -1'])
    # module = Module03(img_name, 3, 3)

    rmse = module.main()
    print(rmse)
